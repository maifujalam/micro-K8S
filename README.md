**This Project setups kubernetes cluster in GCP with 1 master and 2 worker nodes**

1. Update gcp inventory at host_gcp file with ssh-user and key path 
2. Update global vars with cluster details
3. ansible-playbook global-vars-configure.yml -i host_gcp
4. ansible-playbook main.yml -i host_local2
5. 